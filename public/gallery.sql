-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 19 oct. 2021 à 21:05
-- Version du serveur : 5.7.31
-- Version de PHP : 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gallery`
--
CREATE DATABASE IF NOT EXISTS `gallery` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gallery`;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(15, 'Abstract'),
(16, 'Cityscape'),
(17, 'Historical'),
(18, 'Landscape'),
(19, 'Portrait'),
(20, 'Still-Life'),
(21, 'Wild-Life'),
(22, 'Modern');

-- --------------------------------------------------------

--
-- Structure de la table `commentary`
--

DROP TABLE IF EXISTS `commentary`;
CREATE TABLE IF NOT EXISTS `commentary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `painting_id` int(11) DEFAULT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_1CAC12CAB00EB939` (`painting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commentary`
--

INSERT INTO `commentary` (`id`, `painting_id`, `author`, `content`, `created_at`) VALUES
(1, 1, 'Toto', 'Maximus! Maximus! Maximus!', '2021-10-19 17:32:10'),
(2, 1, 'Max', 'My name is Maximus Decimus Meridius!', '2021-10-19 17:36:34'),
(3, 1, 'The Rock', 'Those guys are comedians!', '2021-10-19 17:42:09'),
(4, 1, 'John Cena', 'And you are not perhaps?', '2021-10-19 17:45:34'),
(5, 8, 'Charlie', 'I found Charlie.', '2021-10-19 19:52:35');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211007094635', '2021-10-07 09:48:48', 151),
('DoctrineMigrations\\Version20211014055743', '2021-10-14 05:58:22', 168);

-- --------------------------------------------------------

--
-- Structure de la table `painting`
--

DROP TABLE IF EXISTS `painting`;
CREATE TABLE IF NOT EXISTS `painting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_66B9EBA012469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `painting`
--

INSERT INTO `painting` (`id`, `category_id`, `title`, `description`, `created`, `height`, `width`, `image`) VALUES
(1, 17, 'Alias nam itaque vel.', 'Voluptas magni quasi unde qui. Blanditiis illum qui quas et. Porro ut at aut saepe voluptatem et qui.', '2021-10-14 15:57:07', 190, 126, '1.jpg'),
(2, 17, 'Beatae officia numquam quis.', 'Officia maxime maiores mollitia distinctio voluptas voluptatem fugit.', '2021-10-14 15:57:07', 115, 124, '2.jpg'),
(3, 20, 'Corrupti ut ab eum.', 'Voluptatem pariatur sed accusantium sunt quae debitis eum. Velit aliquam omnis dignissimos.', '2021-10-14 15:57:07', 107, 139, '3.jpg'),
(4, 16, 'Qui qui sunt aliquid.', 'Accusamus qui sint vitae facere minima consectetur hic. Voluptatum est optio qui qui et dolorem. ', '2021-10-14 15:57:07', 161, 186, '4.jpg'),
(5, 19, 'Non quo.', 'Accusamus tempore sunt animi. Itaque et cupiditate debitis libero exercitationem assumenda.', '2021-10-14 15:57:07', 171, 112, '5.jpg'),
(6, 19, 'Non maxime architecto.', 'Optio dolorum ut eum temporibus. Sunt iusto quae temporibus corrupti.', '2021-10-14 15:57:07', 138, 132, '6.jpg'),
(7, 18, 'Aut enim id ratione.', 'Maiores autem soluta fugiat porro sed corporis ab aut. Voluptatum aperiam exercitationem id ex illum.', '2021-10-14 15:57:07', 146, 133, '7.jpg'),
(8, 17, 'Accusamus voluptatem aut.', 'Ratione voluptatum et est temporibus. Nihil sit numquam enim id in.', '2021-10-14 15:57:07', 142, 101, '8.jpg'),
(9, 17, 'Rem impedit saepe repellendus.', 'Quis ipsa exercitationem ut. Architecto nam exercitationem velit cum et.', '2021-10-14 15:57:07', 193, 147, '9.jpg'),
(10, 16, 'Neque fugiat natus.', 'Perspiciatis quaerat recusandae expedita ab et molestiae magnam.', '2021-10-14 15:57:07', 191, 107, '10.jpg'),
(11, 17, 'Quis rerum ex itaque distinctio.', 'Officia qui non ab. Provident sit qui consequuntur quia similique commodi voluptatem ut.', '2021-10-14 15:57:07', 200, 141, '11.jpg'),
(12, 16, 'Veritatis dolorem aut.', 'Ea voluptatibus quae voluptatem aut facere. Quia sit est est natus nesciunt sed ea.', '2021-10-14 15:57:07', 200, 114, '12.jpg'),
(15, 15, 'By Default', 'Fatality', '2021-10-18 17:26:47', 192, 108, 'default.jpg');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentary`
--
ALTER TABLE `commentary`
  ADD CONSTRAINT `FK_1CAC12CAB00EB939` FOREIGN KEY (`painting_id`) REFERENCES `painting` (`id`);

--
-- Contraintes pour la table `painting`
--
ALTER TABLE `painting`
  ADD CONSTRAINT `FK_66B9EBA012469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
