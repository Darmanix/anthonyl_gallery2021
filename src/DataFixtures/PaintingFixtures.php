<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PaintingFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create();
        $categories = $manager->getRepository(Category::class)->findAll();
        $nbCat = count($categories);

        for($i = 1; $i <= 12; $i++){
            $painting = new Painting();
            $painting->setCategory($categories[$faker->numberBetween(0, $nbCat - 1)]);
            $painting->setTitle($faker->sentence(3, true));
            $painting->setDescription($faker->paragraph(3, true));
            $painting->setCreated(new \DateTime());
            $painting->setHeight($faker->numberBetween(100, 200));
            $painting->setWidth($faker->numberBetween(100, 200));
            $painting->setImage($i.'.jpg');
            $manager->persist($painting);
        }

        $manager->flush();
    }

}