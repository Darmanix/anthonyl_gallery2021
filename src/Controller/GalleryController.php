<?php

namespace App\Controller;

use App\Entity\Commentary;
use App\Entity\Painting;
use App\Form\CommentaryType;
use App\Repository\CategoryRepository;
use App\Repository\CommentaryRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    /**
     * @param CategoryRepository $categoryRepository
     * @param PaintingRepository $paintingRepository
     * @return Response
     */
    #[Route('/gallery', name: 'gallery')]
    public function gallery(CategoryRepository $categoryRepository, PaintingRepository $paintingRepository): Response
    {
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $paintings = $paintingRepository->findBy(
            [],
            ['title' => 'ASC']
        );
        return $this->render('pages/gallery.html.twig', [
            'categories' => $categories,
            'paintings' => $paintings
        ]);
    }

    /**
     * @param Painting $painting
     * @param CommentaryRepository $commentaryRepository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/painting/{id}', name: 'painting')]
    public function painting(Painting $painting, CommentaryRepository $commentaryRepository, Request $request, EntityManagerInterface $manager): Response
    {
        $commentaries = $commentaryRepository->findBy([
            'painting' => $painting
        ]);

        $commentary = new Commentary();
        $form = $this->createForm(CommentaryType::class, $commentary);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $commentary->setPainting($painting);
            $commentary->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($commentary);
            $manager->flush();
            return $this->redirectToRoute('gallery');
        }
        return $this->render('pages/detail.html.twig', [
            'painting'      => $painting,
            'commentaries'  => $commentaries,
            'form'          => $form->createView()
            ]);
    }
}
