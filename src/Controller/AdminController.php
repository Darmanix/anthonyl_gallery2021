<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Painting;
use App\Form\CategoryType;
use App\Form\PaintingType;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @param PaintingRepository $paintingRepository
     * @return Response
     */
    #[Route('/admin', name: 'admin')]
    public function admin(PaintingRepository $paintingRepository): Response
    {
        $paintings = $paintingRepository->findAll();
        return $this->render('pages/admin.html.twig', [
            'paintings' => $paintings
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/addpainting', name: 'addpainting')]
    public function addpainting(Request $request, EntityManagerInterface $manager): Response
    {
        $painting = new Painting();
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $painting->setImage('default.jpg');
            $painting->setCreated(new \DateTimeImmutable());
            $manager->persist($painting);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/add.html.twig', ['form' => $form]);
    }

    /**
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    #[Route('/delpainting/{id}', name: 'delpainting')]
    public function delpainting(Painting $painting, EntityManagerInterface $manager): RedirectResponse
    {
        $manager->remove($painting);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }

    /**
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/editpainting/{id}', name: 'editpainting')]
    public function editpainting(Painting $painting, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $painting->setImage('default.jpg');
            $painting->setCreated(new \DateTimeImmutable());
            $manager->persist($painting);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/edit.html.twig', ['form' => $form]);
    }

    #[Route('/addcategory', name: 'addcategory')]
    public function addcategory(Request $request, EntityManagerInterface $manager): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($category);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/category.html.twig', ['form' => $form]);
    }
}