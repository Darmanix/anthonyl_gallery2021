<?php

namespace App\Form;

use App\Entity\Painting;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaintingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre du tableau'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Déscription du tableau'
            ])
            // ->add('created')
            ->add('height', TextType::class, [
                'label' => 'Hauteur du tableau en cm'
            ])
            ->add('width', TextType::class, [
                'label' => 'Largeur du tableau en cm'
            ])
            //->add('image')
            ->add('category', EntityType::class, [
                'label'         => 'Catégorie du tableau',
                'placeholder'   => 'Sélectionnez une catégorie',
                'class'         => 'App:Category',
                'choice_label'  => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
